/*
 * Copyright 2013-2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.cloud.gateway.route;

import org.assertj.core.util.Maps;
import org.junit.Test;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Stefan Stus
 */
public class RouteDefinitionTest {

	// 测试map.putAll
	@Test
	public void addRouteDefinitionKeepsExistingMetadata() {
		Map<String, Object> originalMetadata = Maps.newHashMap("key", "value");
		Map<String, Object> newMetadata = Maps.newHashMap("key2", "value2");

		RouteDefinition routeDefinition = new RouteDefinition();
		routeDefinition.setMetadata(originalMetadata);
		// map.putAll(newMap) 将newMap中的元素复制到map中，即合并map和newMap的元素，取两个集合的并集，两个集合合并
		routeDefinition.getMetadata().putAll(newMetadata);
		System.out.println(routeDefinition);

		assertThat(routeDefinition.getMetadata()).hasSize(2).containsAllEntriesOf(originalMetadata)
				.containsAllEntriesOf(newMetadata);
	}

	// 测试重复setMetaData
	@Test
	public void setRouteDefinitionReplacesExistingMetadata() {
		Map<String, Object> originalMetadata = Maps.newHashMap("key", "value");
		Map<String, Object> newMetadata = Maps.newHashMap("key2", "value2");

		RouteDefinition routeDefinition = new RouteDefinition();
		// 这里直接传递map引用，因此重复设值会修改metaData的引用值，值为最后一次设置的map对象
		routeDefinition.setMetadata(originalMetadata);
		routeDefinition.setMetadata(newMetadata);
		System.out.println(routeDefinition);

		assertThat(routeDefinition.getMetadata()).isEqualTo(newMetadata);
	}

	// 测试map.put
	@Test
	public void addSingleMetadataEntryKeepsOriginalMetadata() {
		Map<String, Object> originalMetadata = Maps.newHashMap("key", "value");

		RouteDefinition routeDefinition = new RouteDefinition();
		routeDefinition.setMetadata(originalMetadata);
		// map.put("key", "value") 将元素key-value添加到map中，即map添加新元素
		routeDefinition.getMetadata().put("key2", "value2");
		System.out.println(routeDefinition);

		assertThat(routeDefinition.getMetadata()).hasSize(2).containsAllEntriesOf(originalMetadata)
				.containsEntry("key2", "value2");
	}

}
